#!/usr/bin/python

# ARPA (Doug Paul) format
# http://www.speech.sri.com/projects/srilm/manpages/ngram-format.html

# Two ways of dealing with low counts:
# SRILM: don't multiply zeros in for unknown words
# Pharaoh: cap at a minimum score exp(-10), including unknown words

import sys, re
import model, log

START = "<s>"
STOP = "</s>"
UNKNOWN = "<unk>"

LOGZERO = -99.0 # same as SRI-LM

class LanguageModel(model.Model):
    def __init__(self, read):
        model.Model.__init__(self)

        self.ceiling = -LOGZERO
        self.remap_unknown = 0

        self.prob = {}
        self.backoff = {}
        self.order = 0

        if type(read) is str:
            self.read(open(read, "r"))
        else:
            self.read(read)

        # for decoder interface
        self.stateless = 0
        
    def read(self, file):
        self.prob.clear()
        self.backoff.clear()
        self.order = 0

        if log.level >= 1:
            log.write("Reading language model...")
        line = file.readline()
        n = None
        total = progress = 0
        heading_re = re.compile(r"\s*\\(\d+)-grams:\s*$")
        headingline_re = re.compile(r"\s*ngram (\d+)=(\d+)\s*$")
        while line != "" and line.strip() != "\\end\\":
            if line.lstrip().startswith("\\"):
                m = heading_re.match(line)
                if m != None:
                    n = int(m.group(1))
                elif line.strip() == "\\data\\":
                    n = "data"
            elif n == "data":
                m = headingline_re.match(line)
                if m is not None:
                    self.order = max(self.order, int(m.group(1)))
                    total += int(m.group(2))
            elif type(n) is int and n <= self.order:
                tokens = line.split()
                n_tokens = len(tokens)
                if n_tokens > n:
                    words = tuple(tokens[1:n+1])
                    self.prob[words] = float(tokens[0])
                    if len(tokens) > n+1:
                        self.backoff[words] = float(tokens[n+1])
                    progress += 1
                    if log.level >= 1 and progress*10/total > (progress-1)*10/total:
                        log.write("...%d0%%" % (progress*10/total))
                elif n_tokens > 0:
                    log.write("Couldn't interpret line in language model: " + line)

            line = file.readline()
        if log.level >= 1:
            log.writeln()

    def write(self, file):
        histogram = {}
        for ngram in self.prob.iterkeys():
            n = len(ngram)
            if not histogram.has_key(n):
                histogram[n] = 0
            histogram[n] += 1
        file.write("\n\\data\\\n")
        lengths = histogram.keys()
        lengths.sort()
        for i in lengths:
            file.write("ngram %d=%d\n" % (i, histogram[i]))
        for i in lengths:
            file.write("\n\\%d-grams:\n" % i)
            for (ngram, count) in self.prob.iteritems():
                n = len(ngram)
                if n == i:
                    file.write("%f\t%s" % (count, " ".join(ngram)))
                    backoff = self.backoff.get(ngram, None)
                    if backoff is not None:
                        file.write("\t%f" % backoff)
                    file.write("\n")
        file.write("\n\\end\\\n")

    def lookup_ngram(self, w):
        if self.remap_unknown:
            w = list(w)
            for i in xrange(len(w)):
                if not self.prob.has_key(w[i]):
                    w[i] = UNKNOWN
            w = tuple(w)

        if self.prob.has_key(w):
            return self.prob[w]
        else:
            context = w[:-1]
            if len(w) == 1:  # 0-gram implicitly has backoff weight of zero
                return LOGZERO 
            else:
                backoff = self.backoff.get(context, 0.0)
                return backoff + self.lookup_ngram(w[1:])

    def lookup_words(self, context, words): # return a negative log prob
        sum = 0.0
        cw = context+words
        for i in xrange(len(context), len(cw)):
            ngram = cw[max(0,i+1-self.order):i+1]
            p = min(self.ceiling, -self.lookup_ngram(ngram))
            sum += p
            #if p < -LOGZERO: sum += p # SRILM
        return sum

    ### Decoder interface
    
    def input(self, fwords):
        self.initial = (START,)

    def transition (self, mstate, context, transition):
        if transition is not model.TO_GOAL:
            #((i,j), ewords, pm_scores) = transition
            ewords = transition[1]
            cost = self.lookup_words(context, ewords)
            n = self.order-1
            if n <= len(ewords) and n > 0:
                newcontext = ewords[-n:]
            else:
                n -= len(ewords)
                if n > 0:
                    newcontext = context[-n:]+ewords
                else:
                    newcontext = ()
            return (newcontext, cost)
        else:
            return ((STOP,), self.lookup_words(context, (STOP,)))

    def estimate_transition (self, transition):
        return self.transition(None, (), transition)[1]

if __name__ == "__main__":
    import fileinput
    lm = LanguageModel(fileinput.input())
